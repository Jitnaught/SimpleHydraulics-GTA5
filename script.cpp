#include "script.h"
#include <sstream>
#include <string>
#include <iomanip>
#include <algorithm>
#include <cctype>
#include <ctime>

using namespace std;

#pragma warning(disable : 4244 4305) // double <-> float conversions

const LPCSTR INI_LOCATION = ".\\SimpleHydraulics.ini", INI_SECTION = "SETTINGS";

int toggleHoldKey, togglePressKey, jumpKey, controllerButton /* 73 */;
float zMulti; //1.5
uint maxTimeHoldButton; //750
bool controllerSupport;

bool enabled = true, toggleKeysWerePressed = false;
DWORD timePressed = 0, amountOfTimePressed = 0, lastTimeRan = 0;

bool isKeyPressed(int nVirtKey)
{
	return (GetAsyncKeyState(nVirtKey) & 0x8000) != 0;
}

std::string subtitle;
DWORD subtitleEndTicks;

void updateSubtitle()
{
	if (GetTickCount() < subtitleEndTicks)
	{
		UI::SET_TEXT_FONT(0);
		UI::SET_TEXT_SCALE(0.55, 0.55);
		UI::SET_TEXT_COLOUR(255, 255, 255, 255);
		UI::SET_TEXT_WRAP(0.0, 1.0);
		UI::SET_TEXT_CENTRE(1);
		UI::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);
		UI::SET_TEXT_EDGE(1, 0, 0, 0, 205);

		UI::BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING");
		UI::ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME((char *)subtitle.c_str());
		UI::END_TEXT_COMMAND_DISPLAY_TEXT(0.5, 0.9);
	}
}

void showSubtitle(std::string str, DWORD time = 2500)
{
	subtitle = str;
	subtitleEndTicks = GetTickCount() + time;
}

string getINIKey(LPCSTR section, LPCSTR keyName, LPCSTR defaultValue, DWORD bufferSize)
{
	char *buffer = new char[bufferSize + 1];
	GetPrivateProfileString(section, keyName, defaultValue, buffer, bufferSize, INI_LOCATION);
	return string(buffer);
}

bool stob(std::string str) 
{
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	std::istringstream is(str);
	bool b;
	is >> std::boolalpha >> b;
	return b;
}

void main()
{
	zMulti = std::stof(getINIKey(INI_SECTION, "Z_MULTIPLIER", "1.0", 6));
	maxTimeHoldButton = std::stoi(getINIKey(INI_SECTION, "MAX_TIME_HOLD_JUMP_BUTTON", "750", 5), nullptr, 0);
	toggleHoldKey = std::stoi(getINIKey(INI_SECTION, "TOGGLE_MOD_HOLD_KEY", "0xA3", 5), nullptr, 0); //Right CTRL
	togglePressKey = std::stoi(getINIKey(INI_SECTION, "TOGGLE_MOD_PRESS_KEY", "0x48", 5), nullptr, 0); //H
	jumpKey = std::stoi(getINIKey(INI_SECTION, "JUMP_KEY", "0x58", 6), nullptr, 0); //X
	controllerSupport = stob(getINIKey(INI_SECTION, "CONTROLLER_SUPPORT", "true", 5));
	controllerButton = std::stoi(getINIKey(INI_SECTION, "JUMP_CONTROLLER_BUTTON", "73", 3)); //vehicle duck

	while (true)
	{
		if (GetTickCount() >= lastTimeRan + 50)
		{
			if (isKeyPressed(toggleHoldKey) && isKeyPressed(togglePressKey))
			{
				toggleKeysWerePressed = true;
			}
			else if (toggleKeysWerePressed)
			{
				toggleKeysWerePressed = false;

				showSubtitle(string("Hydraulics ") + ((enabled = !enabled) ? "enabled" : "disabled"));
			}
			
			if (isKeyPressed(jumpKey) || (controllerSupport && !CONTROLS::_IS_INPUT_DISABLED(2) && CONTROLS::IS_CONTROL_PRESSED(2, controllerButton)))
			{
				if (enabled && timePressed == 0)
				{
					Ped me = PLAYER::PLAYER_PED_ID();

					if (ENTITY::DOES_ENTITY_EXIST(me) && ENTITY::GET_ENTITY_HEALTH(me) > 0 && PED::IS_PED_IN_ANY_VEHICLE(me, false))
					{
						timePressed = GetTickCount();
					}
				}
			}
			else if (timePressed != 0)
			{
				if (enabled)
				{
					amountOfTimePressed = GetTickCount() - timePressed;
					if (amountOfTimePressed > maxTimeHoldButton) amountOfTimePressed = maxTimeHoldButton;

					Ped me = PLAYER::PLAYER_PED_ID();

					if (ENTITY::DOES_ENTITY_EXIST(me) && ENTITY::GET_ENTITY_HEALTH(me) > 0 && PED::IS_PED_IN_ANY_VEHICLE(me, false))
					{
						Vehicle veh = PED::GET_VEHICLE_PED_IS_USING(me);

						if (ENTITY::DOES_ENTITY_EXIST(veh) && VEHICLE::IS_VEHICLE_ON_ALL_WHEELS(veh))
						{
							Vector3 minDimensions, maxDimensions;
							GAMEPLAY::GET_MODEL_DIMENSIONS(ENTITY::GET_ENTITY_MODEL(veh), &minDimensions, &maxDimensions);

							ENTITY::APPLY_FORCE_TO_ENTITY(veh, 0, 0, 0, (amountOfTimePressed + maxDimensions.y * 10) * zMulti, 0, 0, 0, 0, true, true, true, false, true);
						}
					}
				}

				timePressed = 0;
			}
		}

		updateSubtitle();

		WAIT(0);
	}
}

void ScriptMain()
{
	srand(GetTickCount());
	main();
}
